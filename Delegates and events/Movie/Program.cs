﻿using Movie;

class Program
{
    static void Main(string[] args)
    {
        Video video = new Video { Title = "video 1"};
        VideoEncoder videoeEncoder = new ();

        MailService mailService = new ();
        MessageService messageService = new ();

        //videoeEncoder.VideoEncoded += mailService.SendMail_OnVideoEncoded;
        //videoeEncoder.VideoEncoded += messageService.SendMsg_OnVideoEncoded;

        //subscribing with annonymous method
        videoeEncoder.VideoEncoded += delegate (object s, VideoEventArgs e)
        {
            mailService.SendMail_OnVideoEncoded(s, e);
            messageService.SendMsg_OnVideoEncoded(s, e);
        };

        videoeEncoder.Encode(video);
    }
}
