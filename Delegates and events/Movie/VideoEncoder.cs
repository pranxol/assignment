﻿namespace Movie
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }

    //publisher class
    public class VideoEncoder
    {
        //public delegate void VideoEncoderEventHandler(object source, VideoEventArgs args);
        //public event VideoEncoderEventHandler VideoEncoded;

        //declaring event using built-in EventHandler delegate
        public event EventHandler<VideoEventArgs>? VideoEncoded;

        public void Encode(Video video)
        {
            var eventData = new VideoEventArgs();
            eventData.Video = video;

            Console.WriteLine("Encoding Video...");
            Thread.Sleep(3000);

            OnVideoEncoded(eventData);
        }

        protected virtual void OnVideoEncoded(VideoEventArgs e)
        {
            if(VideoEncoded != null)
            {
                VideoEncoded.Invoke(this, e); //invoking all methods of declared event, raising events
                
                // we can also write avoiding the Invoke word like the below line
                //VideoEncoded(this, new VideoEventArgs() { Video = video });
            }
        }
    }
}
