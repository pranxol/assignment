﻿using Movie;

class Program
{
    static void Main(string[] args)
    {
        Video video = new Video { Title = "video 1"};
        VideoEncoder videoeEncoder = new (); //publisher

        MessageService messageService = new();
        videoeEncoder.VideoEncoded += messageService.SendMsg_OnVideoEncoded;

	MailService mailService = new();
        videoeEncoder.VideoEncoded += mailService.SendMail_OnVideoEncoded;

        videoeEncoder.Encode(video);
    }
}
