var TouristPlacesArr = [];
var UniqueId = 0;
var SortAsc = 0;

$(document).ready(function () {
    showJsonData();

    $("#submitbtn").on("click", function () {
        var id = $("#uid").val();
        var name = $("#name").val();
        var add = $("#address").val();
        var rat = $("#rating").val();
        var ratingInt = parseInt(rat);
        var hasReqError = validation(name, add, ratingInt, rat);

        if (!hasReqError) {
            populateArr(id, name, add, ratingInt);
        }
    });

    $("#cancelbtn").on("click", function () {
        $("#submitbtn").val("Insert");
        clearForm();
    });

    $("#picture").on("change", function () {
        var picUrl = window.URL.createObjectURL($("#picture")[0].files[0]);

        previewImg(picUrl);
    });

    $("#namesearch").on("keyup", function () {
        var searchText = $("#namesearch").val();

        setSearchFlag(searchText);
    });

    $("#desc").on("click", function () {
        $("#desc").css("background-color", "greenyellow");
        $("#asc").css("background-color", "");
        SortAsc = -1;

        addToTable();
    });

    $("#asc").on("click", function () {
        $("#asc").css("background-color", "greenyellow");
        $("#desc").css("background-color", "");
        SortAsc = 1;

        addToTable();
    });

    $("#ratingnone").on("click", function () {
        $("#asc").css("background-color", "");
        $("#desc").css("background-color", "");
        SortAsc = 0;

        addToTable();
    });

    $("#placeList").on("click", "input", function () {
        var dataId = parseInt($(this).attr("data-id"));
        var dataCmd = $(this).attr("data-cmd");

        if (dataCmd === "del") {
            delRow(dataId);
        }
        else if (dataCmd === "upd") {
            update(dataId);
        }
    });
});

function populateArr(id, name, add, ratingInt) {
    var pic = "";
    if ($("#picture").val() != "") {
        pic = window.URL.createObjectURL($("#picture")[0].files[0]);
    }

    if (id != "") {
        $("#submitbtn").val("Insert");
        TouristPlacesArr[id].placeName = name;
        TouristPlacesArr[id].placeAddress = add;
        TouristPlacesArr[id].placeRating = ratingInt;
        if (pic != "") {
            TouristPlacesArr[id].placePic = pic;
        }
    }
    else {
        var ob = {
            id: UniqueId,
            placeName: name,
            placeAddress: add,
            placeRating: ratingInt,
            placePic: pic,
            flag: 0
        };

        UniqueId++;
        TouristPlacesArr.push(ob);
    }

    var searchText = $("#namesearch").val();

    setSearchFlag(searchText);
    clearForm();
}

function setSearchFlag(searchText) {
    if (searchText === "") {
        for (var i = 0; i < TouristPlacesArr.length; i++) {
            TouristPlacesArr[i].flag = 1;
        }
    }
    else {
        for (var i = 0; i < TouristPlacesArr.length; i++) {
            if (searchSubstring(searchText, TouristPlacesArr[i].placeName)) {
                TouristPlacesArr[i].flag = 1;
            }
            else {
                TouristPlacesArr[i].flag = 0;
            }
        }
    }

    addToTable();
}

function searchSubstring(searchText, placeName) {
    var flag = false;
    let i, j, k;

    for (i = 0; i < placeName.length; i++) {
        for (j = 0, k = i; j < searchText.length && k < placeName.length; j++, k++) {
            if (placeName[k] != searchText[j])
                break;
        }
        if (j === searchText.length) {
            flag = true;
            break;
        }
    }

    return flag;
}

function validation(name, add, ratingInt, rat) {
    var errorFlag = false;

    if (name === '') {
        $("#nameRequired").html("Name Requied<br>");
        errorFlag = true;
    }
    else {
        $("#nameRequired").text("");
    }

    if (add === '') {
        $("#addressRequired").html("Address Requied<br>");
        errorFlag = true;
    }
    else {
        $("#addressRequired").text("");
    }

    if ((rat === '') || (ratingInt > 5) || (ratingInt < 0)) {
        $("#ratingRequired").html("Rating Required (0 to 5)<br>");
        errorFlag = true;
    }
    else {
        $("#ratingRequired").text("");
    }

    return errorFlag;
}

function showJsonData() {
    $.getJSON("./jsondata.json", function (data) {
        for (var i = 0; i < data.length; i++) {
            var ob = {
                id: UniqueId,
                placeName: data[i].placeName,
                placeAddress: data[i].address,
                placeRating: data[i].rating,
                placePic: data[i].picture,
                flag: 1
            };
            UniqueId++;
            TouristPlacesArr.push(ob);
        }

        addToTable();
    })
        .fail(function () {
            console.log("An error has occurred.");
        });
}

function addToTable() {
    $('#placeListBody').html("");
    var tmpArr = [];

    for (var i = 0; i < TouristPlacesArr.length; i++) {
        tmpArr.push(TouristPlacesArr[i]);
    }

    var len = tmpArr.length;

    if (SortAsc != 0) {
        tmpArr.sort(function (a, b) { return SortAsc * (a.placeRating - b.placeRating) });
    }

    for (var i = 0; i < len; i++) {
        if (tmpArr[i].flag === 1) {
            var tdPlaceName = "<td>" + tmpArr[i].placeName + "</td>";
            var tdPlaceAddress = "<td>" + tmpArr[i].placeAddress + "</td>";
            var tdPlaceRating = "<td>" + tmpArr[i].placeRating + "</td>";
            var tdPlacePic = `<td><img src="${tmpArr[i].placePic}" width="60px" height="60px"></td>`;
            var tdPlaceUpdBtn = '<td class="btnalign"><input class="updatebtn"  type="button" data-cmd="upd" data-id=' + `${tmpArr[i].id} value="Update"></td>`;
            var tdPlacedltBtn = '<td class="btnalign"><input class="deletebtn" type="button" data-cmd="del" data-id=' + `${tmpArr[i].id} value="Delete"></td>`;
            var placeData = "<tr >" + tdPlaceName + tdPlaceAddress + tdPlaceRating + tdPlacePic + tdPlaceUpdBtn + tdPlacedltBtn + "</tr>";

            $("#placeListBody").append(placeData);
        }
    }
}

function delRow(id) {
    for (var i = 0; i < TouristPlacesArr.length; i++) {
        if (TouristPlacesArr[i].id === id)
            break;
    }

    var indx = i;
    TouristPlacesArr.splice(indx, 1);
    addToTable();

    $("#submitbtn").val("Insert");
    clearForm();
}

function update(id) {
    clearForm();

    for (var i = 0; i < TouristPlacesArr.length; i++) {
        if (TouristPlacesArr[i].id === id)
            break;
    }

    var indx = i;
    previewImg(TouristPlacesArr[indx].placePic);

    $("#submitbtn").val("Update");
    $("#uid").val(indx);
    $("#name").val(TouristPlacesArr[indx].placeName);
    $("#address").val(TouristPlacesArr[indx].placeAddress);
    $("#rating").val(TouristPlacesArr[indx].placeRating);
}

function previewImg(picUrl) {
    $("#previewimg").attr("src", picUrl);
    $("#previewimg").removeAttr("class");
    $("#previewimg").attr("class", "img-preview");
}

function cancelPreview() {
    $("#previewimg").removeAttr("src");
    $("#previewimg").removeAttr("class");
    $("#previewimg").attr("class", "img-previewEmpty");
}

function removeRequiredWarning() {
    $("#nameRequired").text("");
    $("#addressRequired").text("");
    $("#ratingRequired").text("");
}

function clearForm() {
    $("#uid").val("");
    $("#name").val("");
    $("#address").val("");
    $("#rating").val("");
    $("#picture").val("");

    removeRequiredWarning();
    cancelPreview();
}