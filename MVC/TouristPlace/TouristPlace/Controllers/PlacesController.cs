﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TouristPlace.Models;

namespace TouristPlace.Controllers
{
    public class PlacesController : Controller
    {
        private PlaceDBContext db = new PlaceDBContext();

        public ActionResult Index(string sortOrder, string searchString)
        {
            TempData["CurrentSortOrder"] = sortOrder;
            TempData["CurrentSearchString"] = searchString;
            TempData["RatingSortParm"]= String.IsNullOrEmpty(sortOrder) ? "rating_desc" : "";
            IEnumerable<Place> places = PlaceNameSearch(searchString);

            switch (sortOrder)
            {
                case "rating_desc":
                    places = places.OrderByDescending(p => p.Rating);
                    break;
                default:
                    places = places.OrderBy(p => p.Rating);
                    break;
            }
            return View(places);
        }

        public IEnumerable<Place> PlaceNameSearch(string searchString)
        {
            IEnumerable<Place> searchResult = db.Places;
            if (!String.IsNullOrEmpty(searchString))
            {
                searchResult = searchResult.Where(s => s.Name.Contains(searchString));
            }
            return searchResult;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Address,Rating,ImagePath,ImageFile")] Place place)
        {
            if (ModelState.IsValid)
            {
                if (place.ImageFile!=null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(place.ImageFile.FileName);
                    string extension = Path.GetExtension(place.ImageFile.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    place.ImagePath = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    place.ImageFile.SaveAs(fileName);

                    db.Places.Add(place);
                    db.SaveChanges();
                    return RedirectToAction("Index", new { sortOrder = TempData["CurrentSortOrder"], searchString = TempData["CurrentSearchString"] });
                }
                else
                {
                    ViewBag.imgrequired = "Image required";
                }
            }
            return View(place);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            Session["pathChk"] = place.ImagePath;

            return View(place);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Address,Rating,ImagePath,ImageFile")] Place place)
        {
            if (ModelState.IsValid)
            {
                if (place.ImageFile != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(place.ImageFile.FileName);
                    string extension = Path.GetExtension(place.ImageFile.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    place.ImagePath = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    place.ImageFile.SaveAs(fileName);
                }
                else
                {
                    place.ImagePath = Session["pathchk"].ToString();
                }
                db.Entry(place).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { sortOrder = TempData["CurrentSortOrder"], searchString = TempData["CurrentSearchString"] });
            }
            return View(place);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Place place = db.Places.Find(id);
            db.Places.Remove(place);
            db.SaveChanges();
            return RedirectToAction("Index", new { sortOrder = TempData["CurrentSortOrder"], searchString = TempData["CurrentSearchString"] });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}