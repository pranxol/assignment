USE MovieBookingDB 

Insert into city Values('Dhaka'), ('Chittagong'), ('Jessore'), ('Khulna');

Select *from city;

Select *from [user];

Insert into [user] Values(1,'Asif','Rahman','Male','1/1/1990');
Insert into [user] Values(1,'Joy','Khan','Male','2/1/1991');
Insert into [user] Values(2,'Fahima','Khan','Female','2/1/1991');
Insert into [user] Values(3,'Rahul','Roy','Male','2/1/1991');
Insert into [user] Values(4,'Dipto','Roy','Male','2/1/1991');
Insert into [user] Values(4,'Akram','khan','Male','2/1/1991');

Select *from [user];

select *from cinema;

Insert into cinema Values(1,'Star Cineplex');
Insert into cinema Values(1,'Blockbuster Cinemas');
Insert into cinema Values(2,'Finlay');
Insert into cinema Values(3,'Monihar');

select *from hall;

Insert into hall Values(1,'Hall-1',250)
Insert into hall Values(1,'Hall-2',200)
Insert into hall Values(2,'Hall-1',200)
Insert into hall Values(2,'Hall-2',300)
Insert into hall Values(3,'Hall-1',300)
Insert into hall Values(5,'Hall-1',300)
Insert into hall Values(5,'Hall-2',250)

--schedule table

Select *from schedule

Alter Table schedule
Alter Column start_time time(0);

Alter Table schedule
Alter Column end_time time(0);

Select *from schedule

Insert into schedule Values(1,1,'10:00:00','2:00:00')
Insert into schedule Values(1,2,'10:00:00','2:00:00')
Insert into schedule Values(1,3,'10:00:00','2:00:00')
Insert into schedule Values(1,4,'10:00:00','2:00:00')
Insert into schedule Values(1,5,'10:00:00','2:00:00')
Insert into schedule Values(1,6,'10:00:00','2:00:00')
go	

Create Procedure spInsertintoSchedule
@Hall_id int, @Day int, @Start_time time, @End_time time
As
Begin
	DECLARE @count INT;
	SET @count = 1;
	While @count<7
	Begin
		Insert into schedule Values(@Hall_id,@Day, @Start_time, @End_time )
		SET @count = @count + 1;
		SET @Day = @Day + 1;
	End
End

drop procedure spInsertintoSchedule

exec spInsertintoSchedule 1,1,'3:00:00','7:00:00'
exec spInsertintoSchedule 2,1,'10:00:00','2:00:00'
exec spInsertintoSchedule 2,1,'3:00:00','7:00:00'
exec spInsertintoSchedule 3,2,'10:00:00','2:00:00'
exec spInsertintoSchedule 4,2,'3:00:00','7:00:00'

Select *from schedule

--movie table

Select *from movie

Alter Table movie
Alter Column release_date date;

Insert into movie Values('Feluda','2020/1/1',5)
Insert into movie Values('Fast and Furious','2019/2/1',5)
Insert into movie Values('Dhaka Attack','2018/2/1',4)
Insert into movie Values('Monpura','2015/6/1',5)

--movie schedule table

select *from movie_schedule
Select *from schedule

Alter Table movie_schedule
Alter Column [date] date;

Insert into movie_schedule values(1,1,'2021/10/24' ,500)
Insert into movie_schedule values(1,3,'2021/10/26' ,400)
Insert into movie_schedule values(1,5,'2021/10/28' ,400)
Insert into movie_schedule values(1,7,'2021/10/24' ,500)
Insert into movie_schedule values(1,9,'2021/10/26' ,400)
Insert into movie_schedule values(1,11,'2021/10/28' ,400)

Insert into movie_schedule values(2,2,'2021/10/25' ,500)
Insert into movie_schedule values(2,4,'2021/10/27' ,400)
Insert into movie_schedule values(2,6,'2021/10/29' ,400)
Insert into movie_schedule values(2,8,'2021/10/25' ,500)
Insert into movie_schedule values(2,10,'2021/10/27' ,400)
Insert into movie_schedule values(2,12,'2021/10/29' ,400)

Insert into movie_schedule values(3,25,'2021/10/25' ,400)
Insert into movie_schedule values(3,27,'2021/10/27' ,400)
Insert into movie_schedule values(3,29,'2021/10/29' ,400)
Insert into movie_schedule values(3,31,'2021/10/25' ,500)
Insert into movie_schedule values(3,33,'2021/10/27' ,400)
Insert into movie_schedule values(3,35,'2021/10/29' ,400)
Insert into movie_schedule values(2,36,'2021/10/30' ,400)

Insert into movie_schedule values(1,1,'2021/10/3' ,500)
Insert into movie_schedule values(1,3,'2021/10/5' ,400)
Insert into movie_schedule values(1,5,'2021/10/7' ,400)
Insert into movie_schedule values(1,7,'2021/10/4' ,500)
Insert into movie_schedule values(1,9,'2021/10/6' ,400)
Insert into movie_schedule values(1,11,'2021/10/8' ,400)

--booking

Insert into booking Values(1,1,2)
Insert into booking Values(2,2,3)
Insert into booking Values(13,2,3)

Select *from booking
Select *from hall
Select *from [user]
Select *from movie_schedule
Select *from schedule

