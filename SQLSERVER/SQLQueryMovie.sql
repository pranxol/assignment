USE MovieBookingDB 

--query-1
Select		distinct(movie.[name]) as [movie name],cinema.[name] as cinema
from		movie_schedule
inner join	movie on movie_schedule.movie_id= movie.id
inner join	schedule on movie_schedule.schedule_id=schedule.id 
inner join	hall on schedule.hall_id=hall.id 
inner join	cinema on hall.cinema_id=cinema.id

--query-2
Select		Count(distinct(movie.[name])) as [total movies], cinema.[name] as cinema
from		movie_schedule
inner join	movie on movie_schedule.movie_id= movie.id
inner join	schedule on movie_schedule.schedule_id=schedule.id 
inner join	hall on schedule.hall_id=hall.id 
inner join	cinema on hall.cinema_id=cinema.id
where		Year(movie.release_date)= Year(GETDATE())
Group by	cinema.[name]

--query-3
Select		* from schedule 
Left Join	movie_schedule
on			schedule.id= movie_schedule.schedule_id and movie_schedule.date<GETDATE() 
where		movie_id is null

--function
Create Function HallWeekend(@hall_id int)
Returns int
As
Begin
	Declare @weekdays Table(daynumber int)
	Insert into @weekdays Values (1),(2),(3),(4),(5),(6),(7)
	Return 
	(Select		daynumber from @weekdays w
	Left Join	schedule s
	on			w.daynumber = s.day and s.hall_id=@hall_id
	where		day is null)
End

Select dbo.HallWeekend(1) as weekend

Drop Function HallWeekend
go

--stored procedure
Create Procedure spTicketBook
@UserID int,
@NumberOfTickets int,
@MovieScheduleID int,
@Result int out
as
Begin
	Declare @PurchasedTickets int
	select @PurchasedTickets=sum(number_of_seat) from booking 
	where user_id=@UserID and movie_schedule_id=@MovieScheduleID
	
	if(@PurchasedTickets + @NumberOfTickets > 4)
	Begin
		set @Result= -2
	End

	Else 
	Begin
		Declare @HallCapacity int
		Select  @HallCapacity= capacity from movie_schedule 
							Inner Join schedule on  movie_schedule.schedule_id= schedule.id 
							Inner Join hall on schedule.hall_id=hall.id 
							where movie_schedule.id=@MovieScheduleID
		Declare @BookedSeats int
		Declare @AvailableSeats int
		Select @BookedSeats=sum(number_of_seat) from booking where movie_schedule_id=@MovieScheduleID
		Set @AvailableSeats = @HallCapacity - @BookedSeats
		
		IF(@AvailableSeats < @NumberOfTickets)
		Begin
			Set @Result= -1
		End

		Else
		Begin
			Declare @Price int
			Select @Price= ticket_price from movie_schedule where movie_schedule.id=@MovieScheduleID
			Set @Result= @Price*@NumberOfTickets

			insert into booking(movie_schedule_id,user_id,number_of_seat) values(@MovieScheduleID,@UserID,@NumberOfTickets)
		End
	End
End

declare @BookingResult INT;
Execute spTicketBook 2,3,13,@BookingResult out
print @BookingResult
go

drop proc spTicketBook

--Create Function HallWeekend(@hall_id int)
--Returns int
--As
--Begin
--	declare @cnt int=1;
--	while @cnt<8
--	begin
--		if(not exists(select * from schedule where hall_id=@hall_id and day= @cnt))
--		begin
--			break
--		end
--		set @cnt =@cnt+1
--	end
--	return @cnt
--End
--go
