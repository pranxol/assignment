﻿<%@ Page Title="Form" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddOrUpdate.aspx.vb" Inherits="StudentInfo.AddOrUpdate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br><br>
    Student ID:<br>
    <input type="text" id="id" runat="server">
    <br><br>

    Name:<br>
    <input type="text" id="name" runat="server">
    <br><br>

    Address:<br>
    <input type="text" id="address" runat="server">
    <br><br>

    Class:<br>
    <asp:DropDownList ID="ClassDDL" runat="server">
        <asp:listitem text="One" value="1" ></asp:listitem>
        <asp:listitem text="Two" value="2"></asp:listitem>
        <asp:listitem text="Three" value="3"></asp:listitem>
        <asp:listitem text="Four" value="4"></asp:listitem>
        <asp:listitem text="Five" value="5"></asp:listitem>
    </asp:DropDownList>
    <br><br>

    <div style="float: left">
    Gender:
    </div>

    <div style="float: left">
    <asp:RadioButtonList ID="GenderRadioBtn" runat="server" RepeatDirection="Horizontal" Font-Bold="False">
        <asp:listitem class="radiobtnwidth" text="Male" value="Male"></asp:listitem>
        <asp:listitem class="radiobtnwidth" text="Female" value="Female"></asp:listitem>
    </asp:RadioButtonList>
    </div>
    <br><br>
    
    Email:<br>
    <input type="email" id="email" runat="server">
    <br><br>
    
    <asp:Button ID="SaveBtn" runat="server" Text="Insert" />

</asp:Content>

