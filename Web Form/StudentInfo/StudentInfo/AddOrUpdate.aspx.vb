﻿Imports System
Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlException
Imports System.Data
Imports System.Configuration

Public Class AddOrUpdate
    Inherits Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            Dim studentId As String = Request.QueryString("ID")
            If studentId IsNot Nothing Then
                PopulateFormByStudentId(studentId)
            End If
        End If
    End Sub

    Protected Sub SaveBtn_Click(sender As Object, e As EventArgs) Handles SaveBtn.Click
        If (id.Value = "" Or name.Value = "" Or address.Value = "" Or email.Value = "" Or GenderRadioBtn.SelectedItem Is Nothing Or Convert.ToString(ClassDDL.SelectedItem.Value) Is Nothing) Then
            MsgBox("Please enter the values", MsgBoxStyle.Information, "Warning")

        Else
            Dim handlerObj As New DbHandler()
            Dim parametersDict As New Dictionary(Of String, String)
            parametersDict.Add("@sid", id.Value)
            parametersDict.Add("@sname", name.Value)
            parametersDict.Add("@saddress", address.Value)
            parametersDict.Add("@sclass", ClassDDL.SelectedItem.Value)
            parametersDict.Add("@sgender", GenderRadioBtn.SelectedItem.Value)
            parametersDict.Add("@semail", email.Value)

            If SaveBtn.Text = "Insert" Then
                handlerObj.sqlCmdExecutor("spStdCreate", parametersDict)

            ElseIf SaveBtn.Text = "Update" Then
                handlerObj.sqlCmdExecutor("spStdUpdate", parametersDict)
            End If

            MsgBox("Data stored seccesfully", MsgBoxStyle.Information, "Success")
            id.Value = ""
            name.Value = ""
            address.Value = ""
            email.Value = ""
            GenderRadioBtn.SelectedIndex = -1
            ClassDDL.SelectedIndex = -1
            SaveBtn.Text = "Insert"
        End If
    End Sub

    Protected Sub PopulateFormByStudentId(ByVal studentId As String)
        Dim handlerObj As New DbHandler()
        Dim parametersDict As New Dictionary(Of String, String)
        parametersDict.Add("@sid", studentId)
        Dim dset As New DataSet
        dset = handlerObj.DataAdapterexecutor("spStudentByID", parametersDict)

        id.Value = dset.Tables(0).Rows(0)("id").ToString()
        name.Value = dset.Tables(0).Rows(0)("name").ToString()
        address.Value = dset.Tables(0).Rows(0)("address").ToString()
        ClassDDL.SelectedValue = dset.Tables(0).Rows(0)("class").ToString()
        GenderRadioBtn.SelectedValue = dset.Tables(0).Rows(0)("gender").ToString()
        email.Value = dset.Tables(0).Rows(0)("email").ToString()

        SaveBtn.Text = "Update"
    End Sub
End Class