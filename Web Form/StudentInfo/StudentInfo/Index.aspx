﻿<%@ Page Title="Student List" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.vb" Inherits="StudentInfo.Index" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3 style ="text-align:center">Student List</h3>
    <p style="text-align:center ">
        <asp:LinkButton ID="AddButton" class="AddButtonstyle" href="AddOrUpdate.aspx" runat="server" >Add New</asp:LinkButton>
    </p>
    <br>
    <asp:GridView ID="GridViewStudent"  runat="server" AutoGenerateColumns="False" Width="500px" HeaderStyle-CssClass="centerHeaderText" HorizontalAlign="Center">
       <RowStyle HorizontalAlign="Center" />
        <Columns>
            <asp:BoundField  DataField="id" HeaderText="Student ID" />
            <asp:BoundField  DataField="name" HeaderText="Name" />
            <asp:BoundField  DataField="address" HeaderText="Address" />
            <asp:BoundField  DataField="email" HeaderText="E-mail" />
            <asp:BoundField  DataField="gender" HeaderText="Gender" />
            <asp:BoundField  DataField="class" HeaderText="Class" />
            <asp:templatefield>
                <itemtemplate>
                    <asp:LinkButton ID="EditButton" OnClick="EditButton_Click" CommandArgument='<%#Eval("id") %>' runat="server">Edit</asp:LinkButton>&nbsp&nbsp|&nbsp
                    <asp:LinkButton ID="DeleteButton" OnClick="DeleteButton_Click" CommandArgument='<%#Eval("id") %>' runat="server">Delete</asp:LinkButton>
                 </itemtemplate>
            </asp:templatefield>
        </Columns>
    </asp:GridView>
</asp:Content>