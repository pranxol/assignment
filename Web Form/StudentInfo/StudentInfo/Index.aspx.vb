﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlException
Imports System.Configuration

Public Class Index
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim handlerObj As New DbHandler()
        Dim parametersDict As New Dictionary(Of String, String)
        Dim dset As New DataSet
        dset = handlerObj.DataAdapterexecutor("spStudentList", parametersDict)
        GridViewStudent.DataSource = dset
        GridViewStudent.DataBind()
    End Sub

    Protected Sub EditButton_Click(sender As Object, e As EventArgs)
        Dim lnk As LinkButton = CType(sender, LinkButton)
        Response.Redirect("~/AddOrUpdate.aspx?ID=" + lnk.CommandArgument)
    End Sub

    Protected Sub DeleteButton_Click(sender As Object, e As EventArgs)
        Dim lnk As LinkButton = CType(sender, LinkButton)
        Select Case MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete")
            Case MsgBoxResult.Yes
                Dim handlerObj As New DbHandler()
                Dim parametersDict As New Dictionary(Of String, String)
                parametersDict.Add("@sid", lnk.CommandArgument)
                handlerObj.sqlCmdExecutor("spDeleteById", parametersDict)

                MsgBox("Deleted seccesfully", MsgBoxStyle.Information, "Success")
        End Select
        Response.Redirect("~/Index.aspx")
    End Sub

End Class
