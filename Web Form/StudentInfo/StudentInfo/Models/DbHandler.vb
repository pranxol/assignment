﻿Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlException
Imports System.Configuration
Public Class DbHandler
    Private constr As String = ConfigurationManager.ConnectionStrings("DBCS").ConnectionString
    Public Sub sqlCmdExecutor(ByVal spName As String, parametersDict As Dictionary(Of String, String))
        Using con As New SqlConnection(constr)
            Dim cmd As New SqlCommand()
            cmd = New SqlCommand(spName, con)
            cmd.CommandType = CommandType.StoredProcedure

            For Each pair As KeyValuePair(Of String, String) In parametersDict
                If (pair.Key = "@sid" Or pair.Key = "@class") Then
                    cmd.Parameters.AddWithValue(pair.Key, Convert.ToInt32(pair.Value))
                Else
                    cmd.Parameters.AddWithValue(pair.Key, pair.Value)
                End If
            Next

            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
        End Using
    End Sub

    Public Function DataAdapterexecutor(ByVal spName As String, parametersDict As Dictionary(Of String, String)) As DataSet
        Dim dset As New DataSet
        Using con As New SqlConnection(constr)
            con.Open()
            Dim sda As New SqlDataAdapter()
            sda.SelectCommand = New SqlCommand(spName, con)
            sda.SelectCommand.CommandType = CommandType.StoredProcedure

            For Each pair As KeyValuePair(Of String, String) In parametersDict
                If (pair.Key = "@sid" Or pair.Key = "@class") Then
                    sda.SelectCommand.Parameters.AddWithValue(pair.Key, Convert.ToInt32(pair.Value))
                End If
            Next

            sda.Fill(dset)
            con.Close()
        End Using
        Return dset
    End Function
End Class
