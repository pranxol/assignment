﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudentInfoApi.CourseData;
using StudentInfoApi.Models;

namespace StudentInfoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private ICourseRepository _courseRepository;

        public CoursesController(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        [HttpGet]
        public IActionResult GetCourses()
        {
            return Ok(_courseRepository.GetCourses());
        }

        [HttpGet("{id}")]
        public IActionResult GetCourse(int id)
        {
            var course  = _courseRepository.GetCourse(id);
            if (course != null)
            {
                return Ok(course);
            }
            return NotFound($"course with {id} was not found");
        }

        [HttpPost]
        public IActionResult PostCourse(Course course)
        {
            var existingCourse = _courseRepository.GetCourse(course.CourseId );
            if (existingCourse == null)
            {
                _courseRepository.AddCourse(course);
                return CreatedAtAction(nameof(GetCourse), new { id = course.CourseId }, course);
            }
            return Conflict($"Here is a conflict with the ID={course.CourseId}");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(int id)
        {
            var course = _courseRepository.GetCourse(id);
            if (course != null)
            {
                _courseRepository.DeleteCourse(course );
                return Ok();
            }
            return NotFound($"Course with {id} was not found");
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCourse(int id, Course course)
        {
            if (id != course.CourseId)
            {
                return BadRequest();
            }
            var existingCourse = _courseRepository.GetCourse(id);
            if (existingCourse != null)
            {
                _courseRepository.UpdateCourse(course);
                return Ok();
            }
            return NotFound();
        }
    }
}
