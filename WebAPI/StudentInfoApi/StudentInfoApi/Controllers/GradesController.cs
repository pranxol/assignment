﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudentInfoApi.GradeData;
using StudentInfoApi.Models;
using StudentInfoApi.ViewModels;

namespace StudentInfoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GradesController : ControllerBase
    {
        private StudentContext _gradeContext;
        private IGradeRepository _gradeRepository;
        private readonly IMapper _mapper;

        public GradesController(IGradeRepository gradeRepository, StudentContext gradeContext, IMapper Mapper)
        {
            _gradeContext = gradeContext;
            _mapper = Mapper;
            _gradeRepository = gradeRepository;
        }

        [HttpGet]
        public IActionResult GetGrades()
        {
            var allGrade = _gradeRepository.GetGrades();
            List<GradeVM> gradeList = new List<GradeVM>();
            foreach (Grade grade in allGrade )
            {
                var gradeWithStudents = _mapper.Map<GradeVM>(grade);
                gradeWithStudents.Students = _gradeContext.Students.Where(s => s.GradeId == grade.GradeId).Select(s => s.StudentId).ToList();
                gradeList.Add(gradeWithStudents);
            }
            return Ok(gradeList);
        }

        [HttpGet("{id}")]
        public IActionResult GetGrade(int id)
        {
            var grade  = _gradeRepository.GetGrade(id);
            if (grade != null)
            {
                var gradeWithStudents = _mapper.Map<GradeVM>(grade);
                gradeWithStudents.Students = _gradeContext.Students.Where(s => s.GradeId == grade.GradeId).Select(s=>s.StudentId ).ToList ();
                return Ok(gradeWithStudents);
            }
            return NotFound($"grade with {id} was not found");
        }

        [HttpPost]
        public IActionResult PostGrade(Grade grade)
        {
            var existingGrade = _gradeRepository.GetGrade(grade.GradeId );
            if (existingGrade == null)
            {
                _gradeRepository.AddGrade(grade);
                return CreatedAtAction(nameof(GetGrade), new { id = grade.GradeId }, grade);
            }
            return Conflict($"Here is a conflict with the ID={grade.GradeId}");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteGrade(int id)
        {
            var grade = _gradeRepository.GetGrade(id);
            if (grade != null)
            {
                _gradeRepository.DeleteGrade(grade );
                return Ok();
            }
            return NotFound($"Grade with {id} was not found");
        }

        [HttpPut("{id}")]
        public IActionResult UpdateGrade(int id, Grade grade)
        {
            if (id != grade.GradeId)
            {
                return BadRequest();
            }
            var existingGrade = _gradeRepository.GetGrade(id);
            if (existingGrade != null)
            {
                _gradeRepository.UpdateGrade(grade);
                return Ok();
            }
            return NotFound();
        }
    }
}
