﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudentInfoApi.Models;
using StudentInfoApi.StudentData;
using StudentInfoApi.ViewModels;

namespace StudentInfoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private StudentContext _studentContext;
        private IMapper _mapper;
        private IStudentRepository _studentRepository;
        
        public StudentsController (IStudentRepository studentRepository, StudentContext studentcontext, IMapper Mapper )
        {
            _studentContext = studentcontext;
            _mapper = Mapper;
            _studentRepository = studentRepository;
        }
        
        [HttpGet]
        public IActionResult GetStudents()
        {
            var allStudents = _studentRepository.GetStudents();
            List<StudentVM> studentsList = new List<StudentVM>(); 

            foreach (Student student in allStudents)
            {
                var studentWithCourses = _mapper.Map<StudentVM>(student);

                List<Course> courses = _studentContext.StudentCourses.Where(x => x.StudentId == student.StudentId).Include(x => x.Course).Select(x => x.Course).ToList();
                List<CoursesDetailsVM> mappedCourses = _mapper.Map<List<Course>, List<CoursesDetailsVM>>(courses);
                studentWithCourses.Courses = mappedCourses;

                var studentGrade = _studentContext.Grades.Where(x => x.GradeId == student.GradeId).Select(g => g.GradeName).FirstOrDefault();
                studentWithCourses.Grade = studentGrade;

                studentsList.Add(studentWithCourses);
            }
            return Ok(studentsList);
        }

        [HttpGet("{id}")]
        public IActionResult GetStudent(int id)
        {
            var student = _studentRepository.GetStudent(id);
            if (student != null)
            {             
                var studentWithCourses = _mapper.Map<StudentVM>(student);

                List<Course> courses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course).ToList();
                List<CoursesDetailsVM> mappedCourses = _mapper.Map<List<Course>, List<CoursesDetailsVM>>(courses);
                studentWithCourses.Courses = mappedCourses;

                var studentGrade = _studentContext.Grades.Where(x => x.GradeId == student.GradeId).Select (g=>g.GradeName).FirstOrDefault() ;
                studentWithCourses.Grade = studentGrade;
                
                return Ok(studentWithCourses);
            }
            return NotFound($"Employee with {id} was not found");
        }

        [HttpPost]
        public IActionResult PostStudent(Student student )
        {
            var existingStudent = _studentRepository.GetStudent(student.StudentId);
            if (existingStudent == null)
            {
                _studentRepository.AddStudent(student);
                return CreatedAtAction(nameof(GetStudent), new { id = student.StudentId }, student);
            }
            return Conflict($"Here is a conflict with the ID={student.StudentId}");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteStudent(int id)
        {
            var existingstudent = _studentRepository.GetStudent(id);
            if (existingstudent != null)
            {
                _studentRepository.DeleteStudent(existingstudent);
                return Ok();
            }
            return NotFound($"Employee with {id} was not found");
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStudent(int id, Student student )
        {
            if (id != student.StudentId)
            {
                return BadRequest();
            }
            var existingStudent = _studentRepository.GetStudent(id);
            if(existingStudent !=null)
            {
                _studentRepository.UpdateStudent(student);
                return Ok();
            }
            return NotFound();
        }
    }
}
