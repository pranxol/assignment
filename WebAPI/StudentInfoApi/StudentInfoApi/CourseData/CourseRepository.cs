﻿using StudentInfoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.CourseData
{
    public class CourseRepository : ICourseRepository
    {
        private StudentContext _courseContext;
        public CourseRepository(StudentContext coursecontext)
        {
            _courseContext = coursecontext;
        }
    
        public Course AddCourse(Course course )
        {
            _courseContext.Courses.Add(course);
            _courseContext.SaveChanges();
            return course;
        }

        public void DeleteCourse(Course course)
        {
            _courseContext.Courses.Remove(course);
            _courseContext.SaveChanges();
        }

        public Course UpdateCourse(Course course)
        {
            var existingCourse = _courseContext.Courses.Find(course.CourseId );
            if (existingCourse != null)
            {
                existingCourse.CourseId = course.CourseId;
                existingCourse.CourseName = course.CourseName;

                _courseContext.Courses.Update(existingCourse);
                _courseContext.SaveChanges();
                return course;
            }
            return null;
        }

        public Course GetCourse(int id)
        {
            var course = _courseContext.Courses.Find(id);
            return course;
        }

        public List<Course> GetCourses()
        {
            return _courseContext.Courses.ToList();
        }
    }
}
