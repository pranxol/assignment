﻿using StudentInfoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.CourseData
{
    public interface ICourseRepository
    {
        List<Course> GetCourses();
        Course GetCourse(int id);
        Course AddCourse(Course course);
        void DeleteCourse(Course course);
        Course UpdateCourse(Course course);
    }
}
