﻿using StudentInfoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.GradeData
{
    public class GradeRepository : IGradeRepository
    {
        private StudentContext _gradeContext;
        public GradeRepository(StudentContext gradeContext)
        {
            _gradeContext = gradeContext;
        }
        public Grade AddGrade(Grade grade)
        {
            _gradeContext.Grades.Add(grade);
            _gradeContext.SaveChanges();
            return grade;
        }

        public void DeleteGrade(Grade grade)
        {
            _gradeContext.Grades.Remove(grade);
            _gradeContext.SaveChanges();
        }

        public Grade GetGrade(int id)
        {
            var grade = _gradeContext.Grades.Find(id);
            return grade;
        }

        public List<Grade> GetGrades()
        {
            return _gradeContext.Grades.ToList();
        }

        public Grade UpdateGrade(Grade grade)
        {
            var existingGrade = _gradeContext.Grades.Find(grade.GradeId);
            if (existingGrade != null)
            {
                existingGrade.GradeId = grade.GradeId;
                existingGrade.GradeName = grade.GradeName ;

                _gradeContext.Grades.Update(existingGrade);
                _gradeContext.SaveChanges();
                return grade;
            }
            return null;
        }
    }
}
