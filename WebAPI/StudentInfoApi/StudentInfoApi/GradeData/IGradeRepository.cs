﻿using StudentInfoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.GradeData
{
    public interface IGradeRepository
    {
        List<Grade> GetGrades();
        Grade GetGrade(int id);
        Grade AddGrade(Grade grade);
        void DeleteGrade(Grade grade);
        Grade UpdateGrade(Grade grade);
    }
}
