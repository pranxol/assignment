﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentInfoApi.Migrations
{
    public partial class couseids_addednewnew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CourseIds",
                table: "Students",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CourseIds",
                table: "Students");
        }
    }
}
