﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentInfoApi.Migrations
{
    public partial class couseids_addednewnewnew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CourseIds",
                table: "Students");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CourseIds",
                table: "Students",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
