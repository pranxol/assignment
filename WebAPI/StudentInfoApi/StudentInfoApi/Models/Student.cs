﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }      
        public IList<StudentCourse> StudentCourses { get; set; }
        public int? GradeId { get; set; }
        public Grade Grade { get; set; }
    }
}
