﻿using AutoMapper;
using StudentInfoApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.Models
{
    public class StudentProfile: Profile
    {
        public StudentProfile() {
            CreateMap<Student, StudentVM>();
            CreateMap<Course, CoursesDetailsVM>();
            CreateMap<Grade, GradeVM>();
        }
    }
}
