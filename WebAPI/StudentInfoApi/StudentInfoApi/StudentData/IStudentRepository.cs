﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentInfoApi.Models;
using StudentInfoApi.ViewModels;

namespace StudentInfoApi.StudentData
{
    public interface IStudentRepository
    {
        List<Student> GetStudents();
        Student GetStudent(int id);
        Student AddStudent(Student student);
        void DeleteStudent(Student student);
        Student UpdateStudent(Student student);
    }
}
