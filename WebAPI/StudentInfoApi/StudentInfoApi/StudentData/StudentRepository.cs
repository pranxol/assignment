﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudentInfoApi.Models;
using StudentInfoApi.ViewModels;

namespace StudentInfoApi.StudentData
{
    public class StudentRepository : IStudentRepository
    {
        private StudentContext _studentContext;
        private  IMapper _mapper;
        public StudentRepository (StudentContext studentcontext, IMapper Mapper)
        {
            _mapper = Mapper;
            _studentContext = studentcontext;
        }

        public Student AddStudent(Student student)
        {
            _studentContext.Students.Add(student);
            _studentContext.SaveChanges();

            return student;
        }

        public void DeleteStudent(Student student)
        {
            _studentContext.Students.Remove(student);
            _studentContext.SaveChanges();
        }

        public Student UpdateStudent(Student student)
        {
            var existingStudent = _studentContext.Students.Find(student.StudentId);
            if(existingStudent !=null)
            {
                List<StudentCourse> toRemove = _studentContext.StudentCourses.Where(r => r.StudentId == student.StudentId).ToList();
                _studentContext.StudentCourses.RemoveRange(toRemove);

                existingStudent.StudentId = student.StudentId;
                existingStudent.Name = student.Name;
                existingStudent.Class = student.Class;
                existingStudent.Gender = student.Gender;
                existingStudent.StudentCourses = student.StudentCourses;
                existingStudent.GradeId = student.GradeId;
                _studentContext.Students.Update(existingStudent);
                _studentContext.SaveChanges();
                return student;
            }
            return null;
        }

        public Student GetStudent(int id)
        {
            var student = _studentContext.Students.Where(x=>x.StudentId==id).FirstOrDefault();
            return student;
        }

        public List<Student> GetStudents()
        {
            return _studentContext.Students.ToList();
        }
    }
}





//student.Courses = _studentContext.StudentCourses.Where(x => x.StudentId == student.StudentId).Include(x => x.Course).Select(x => x.Course).ToList();

//if(student!=null)
//{
//    student.Courses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course).ToList();
//}

//-----------------

//var studentWithCourses = _studentContext.Students.Where(s => s.StudentId == id).Select(student => new StudentVM()
//{
//    StudentId = student.StudentId,
//    Name = student.Name,
//    Class = student.Class,
//    Gender = student.Gender,
//    //Courses = student.StudentCourses.Select(ss => ss.CourseId).ToList()
//}).FirstOrDefault();

//    --------------------------

//public Student GetStudent(int id)
//{
//    //var student = _studentContext.Students.Include("StudentCourses").Include("Courses").Where (x=>x.StudentId==id).FirstOrDefault();
//    var student = _studentContext.Students.Find(id);

//    var StudentWithCourses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course);
//    student.Courses = StudentWithCourses.Select(x => x.Course).ToList();

//    return student;
//}
//-----------------
//student.CoursesTaken = _studentContext.StudentCourses.Where(s => s.StudentId == student.StudentId).Select(sc => sc.CourseId).ToList();
//---------------

//public StudentVM GetStudent(int id)
//{
//    var studentWithCourses = _studentContext.Students.Where(s => s.StudentId == id).Select(student => new StudentVM()
//    {
//        StudentId = student.StudentId,
//        Name = student.Name,
//        Class = student.Class,
//        Gender = student.Gender,
//        Courses = student.StudentCourses.Select(ss => ss.CourseId).ToList()
//        //CoursesTaken = student.StudentCourses.Select(s => s.CourseId).ToList()
//    }).FirstOrDefault();

//    return studentWithCourses;
//}

//var studentWithCourse = _studentContext.Students.Where(s => s.StudentId == id).Select(student => new Student()
//{
//    StudentId = student.StudentId,
//    Name = student.Name,
//    Class = student.Class,
//    Gender = student.Gender,
//    StudentCourses = student.StudentCourses,
//    CoursesTaken = student.StudentCourses.Select(s => s.CourseId).ToList()
//}).FirstOrDefault();


//if (student != null)
//{
//    student.CoursesTaken = _studentContext.StudentCourses.Where(s => s.StudentId == id).Select(sc => sc.CourseId).ToList();
//}


//var studentWithCourse = _studentContext.Students.Where(s => s.StudentId == id).Select(student => new Student()
//{
//    StudentId = student.StudentId,
//    Name = student.Name,
//    Class = student.Class,
//    Gender = student.Gender,
//    StudentCourses = student.StudentCourses.Select(ss=>ss.CourseId)
//    //CoursesTaken = student.StudentCourses.Select(s => s.CourseId).ToList()
//}).FirstOrDefault();

//var student = _studentContext.Students.Find(id);
////student.Courses= (IList<Course>)_studentContext.StudentCourses.Where(s => s.StudentId == id).Select(sc => sc.CourseId).ToList();
//List<int> CoursesTaken = student.StudentCourses.Where(x => x.StudentId == id).Select(s => s.CourseId).ToList();
//student.Courses= _studentContext.Students.Include ("Courses").Include("StudentCourses").Where(x => x.StudentId == id).Select(s=>s.Courses).FirstOrDefault();
//student.Courses = _studentContext.Students.Include("StudentCourses").Include("Courses").Where(x => x.StudentId == id).Select(s=>s.Courses).FirstOrDefault().ToList();




//---------------------------------
//public StudentVM GetStudent(int id)
//{
//    var student = _studentContext.Students.Where(x => x.StudentId == id).FirstOrDefault();
//    if (student != null)
//    {
//        var studentWithCourses = _mapper.Map<StudentVM>(student);
//        List<Course> course = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course).ToList();
//        List<CoursesDetailsVM> mappedCourses = new List<CoursesDetailsVM>();
//        foreach (Course c in course)
//        {
//            CoursesDetailsVM mappedCourse = _mapper.Map<CoursesDetailsVM>(c);
//            mappedCourses.Add(mappedCourse);
//        }
//        //List<CoursesDetailsVM> courses = _mapper.Map<CoursesDetailsVM>(course);
//        //studentWithCourses.Courses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course.CourseId).ToList();
//        studentWithCourses.Courses = mappedCourses;
//        return studentWithCourses;
//    }

//    return null;
//}

//public List<StudentVM> GetStudents()
//{
//    var allStudents = _studentContext.Students.ToList();
//    List<StudentVM> studentsList = new List<StudentVM>();

//    foreach (Student student in allStudents)
//    {
//        var studentWithCourses = _mapper.Map<StudentVM>(student);
//        //studentWithCourses.Courses = _studentContext.StudentCourses.Where(x => x.StudentId == student.StudentId).Include(x => x.Course).Select(x => x.Course.CourseId).ToList();
//        List<Course> course = _studentContext.StudentCourses.Where(x => x.StudentId == student.StudentId).Include(x => x.Course).Select(x => x.Course).ToList();
//        List<CoursesDetailsVM> mappedCourses = new List<CoursesDetailsVM>();
//        foreach (Course c in course)
//        {
//            CoursesDetailsVM mappedCourse = _mapper.Map<CoursesDetailsVM>(c);
//            mappedCourses.Add(mappedCourse);
//        }
//        //List<CoursesDetailsVM> courses = _mapper.Map<CoursesDetailsVM>(course);
//        //studentWithCourses.Courses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course.CourseId).ToList();
//        studentWithCourses.Courses = mappedCourses;
//        studentsList.Add(studentWithCourses);
//    }
//    return studentsList;
//}


//-----------------
//public Student GetStudent(int id)
//{
//    var student = _studentContext.Students.Where(x => x.StudentId == id).FirstOrDefault();
//    return student;

//    //if (student != null)
//    //{
//    //    var studentWithCourses = _mapper.Map<StudentVM>(student);
//    //    List<Course>courses = _studentContext.StudentCourses.Where(x => x.StudentId == id).Include(x => x.Course).Select(x => x.Course).ToList();
//    //    List<CoursesDetailsVM> mappedCourses = _mapper.Map<List<Course>, List<CoursesDetailsVM>>(courses);

//    //    studentWithCourses.Courses = mappedCourses;
//    //    return studentWithCourses;
//    //}

//    //return null;
//}

//public List<Student> GetStudents()
//{
//    var allStudents = _studentContext.Students.ToList();
//    return allStudents;

//    //List<StudentVM> studentsList = new List<StudentVM>(); 

//    //foreach (Student student in allStudents)
//    //{
//    //    var studentWithCourses = _mapper.Map<StudentVM>(student);

//    //    List<Course> courses = _studentContext.StudentCourses.Where(x => x.StudentId == student.StudentId).Include(x => x.Course).Select(x => x.Course).ToList();
//    //    List<CoursesDetailsVM> mappedCourses = _mapper.Map<List<Course>, List<CoursesDetailsVM>>(courses);

//    //    studentWithCourses.Courses = mappedCourses;
//    //    studentsList.Add(studentWithCourses);
//    //}
//    //return studentsList;
//}
//    }
//}

