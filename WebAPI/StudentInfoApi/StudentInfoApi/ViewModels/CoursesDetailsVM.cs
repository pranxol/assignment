﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.Models
{
    public class CoursesDetailsVM
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
