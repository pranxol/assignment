﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.ViewModels
{
    public class GradeVM
    {
        public int GradeId { get; set; }
        public string GradeName { get; set; }
        public List<int> Students { get; set; }
    }
}
