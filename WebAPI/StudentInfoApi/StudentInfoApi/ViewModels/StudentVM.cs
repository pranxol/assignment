﻿using StudentInfoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentInfoApi.ViewModels
{
    public class StudentVM
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public List<CoursesDetailsVM> Courses { get; set; }
        public string Grade { get; set; }
    }
}
