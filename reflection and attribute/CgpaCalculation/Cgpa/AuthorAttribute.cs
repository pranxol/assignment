﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CgpaCalculation
{
    //custom attribute
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class AuthorAttribute : Attribute
    {
        private string _name;
        public double version;

        public AuthorAttribute(string name)
        {
            _name = name;
            version = 1.0;
        }

        public string GetName()
        {
            return _name;
        }
    }
}
