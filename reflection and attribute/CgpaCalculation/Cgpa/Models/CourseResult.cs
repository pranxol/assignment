﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CgpaCalculation.Models
{
    public class CourseResult
    {
        public int StudentId { get; set; }
        public string CourseCode { get; set; }
        public float Grade { get; set; }
    }
}
