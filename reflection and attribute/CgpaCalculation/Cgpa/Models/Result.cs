﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CgpaCalculation.Models
{
    public class Result
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public float CGPA { get; set; }
    }
}
