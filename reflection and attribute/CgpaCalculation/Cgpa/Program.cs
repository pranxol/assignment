﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using CgpaCalculation.Models;

namespace CgpaCalculation
{
    [Author("Pranto Biswas", version = 1.2)]
    class StudentsCgpa
    {
        static void Main(string[] args)
        {
            string filePath = "C:\\Users\\prantokumar\\Desktop\\files\\";

            List<Student> studentList = ReadFromFile<Student>(filePath + "studentinfo.txt");

            List<CourseResult> courseResultList = ReadFromFile<CourseResult>(filePath + "courseresult.txt");
            
            IEnumerable<Result> gradeSheet = CreateResult(studentList,courseResultList);

            DisplayResult(gradeSheet);

            PrintAuthorInfo(typeof(StudentsCgpa));
        }

        public static List<T> ReadFromFile<T>(string filePath)
        {
            Type t = typeof(T);
            List<T> fileDataList = new List<T>();
            var properties= t.GetProperties();
            string line = "";

            using (StreamReader sr = new StreamReader(filePath))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    string[] propertyValues = line.Split(',');
                    var inputObj = Activator.CreateInstance(t);
                    int i = 0;

                    foreach(var property in properties)
                    {
                        string propType = property.PropertyType.Name;

                        if(propType == "Int32")
                        {
                            property.SetValue(inputObj, Convert.ToInt32(propertyValues[i++]));
                        }
                        else if(propType == "Single")
                        {
                            property.SetValue(inputObj, Convert.ToSingle(propertyValues[i++]));
                        }
                        else
                        {
                            property.SetValue(inputObj, propertyValues[i++]);
                        }
                    }
                    fileDataList.Add((T)inputObj);
                }
            }
            return fileDataList;
        }

        public static IEnumerable<Result> CreateResult(List<Student> studentList, List<CourseResult> courseResultList)
        {
            var cgpaList = studentList.GroupJoin(courseResultList,
                                                std => std.Id,
                                                course => course.StudentId,
                                                (std, coursesTaken) => new Result
                                                {
                                                    StudentName = std.Name,
                                                    StudentId = std.Id,
                                                    CGPA = coursesTaken.Count() == 0 ? 0: 
                                                    (float)Math.Round(coursesTaken.Average(c => c.Grade), 2)
                                                });

            return cgpaList;
        }

        public static void DisplayResult(IEnumerable<Result> resultList)
        {
            foreach (Result studentinfo in resultList)
            {
                Console.WriteLine("ID: {0}  Name: {1}  CGPA: {2:0.00}", studentinfo.StudentId, studentinfo.StudentName, studentinfo.CGPA);
            }
        }

        private static void PrintAuthorInfo(Type t)
        {
            Console.WriteLine("\nAuthor information for {0}", t);

            // Using reflection.  
            Attribute[] attrs = Attribute.GetCustomAttributes(t); 
  
            foreach (Attribute attr in attrs)
            {
                if (attr is AuthorAttribute)
                {
                    AuthorAttribute a = (AuthorAttribute)attr;
                    Console.WriteLine("   {0}, version {1:f}", a.GetName(), a.version);
                }
            }
        }
    }
}